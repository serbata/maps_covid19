package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Info:Serializable {
    val nombre: String?=""
    val creado : String?=""
    val fecha:String?=""
    val fechaInicio:String?=""
    val fechaFin:String?=""
    val numeroFechas:Number?=null
    val periodo:String?=""
    val nivelAdministrativo: String?=""
    val nombreLugar:String?=""
    val codigoIsoLugar:String?=""
    val numeroLugares:Number?=null
}