package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Regiones:Serializable {
    val nombre:String?=""
    val nivelAdministrativo:String?=""
    val nombreLugar:String?=""
    val codigoIsoLugar:String?=""
    val long:Number?=null
    val lat: Number?=null
    val data : Data?=null
}