package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Data:Serializable {
    val casosConfirmados:Number?=null
    val casosUci:Number?=null
    val casosFallecidos:Number?=null
    val casosHospitalizados:Number?=null
    val casosRecuperados:Number?=null
    val casosConfirmadosDiario:Number?=null
    val casosUciDiario:Number?=null
    val casosFallecidosDiario:Number?=null
}