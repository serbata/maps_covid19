package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Total:Serializable {
    val trace: Trace?=null
    val timeline: List<Timeline>?=null
}