package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Trace : Serializable {
    val info : Info?=null
    val contacto: Contacto?=null
}