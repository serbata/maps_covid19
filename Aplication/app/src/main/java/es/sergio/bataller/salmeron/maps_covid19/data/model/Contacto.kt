package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Contacto:Serializable {
    val nombreEmpresa:String?=""
    val url:String?=""
    val email:String?=""
}