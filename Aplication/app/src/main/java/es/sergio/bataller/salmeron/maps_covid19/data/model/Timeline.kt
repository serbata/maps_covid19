package es.sergio.bataller.salmeron.maps_covid19.data.model

import java.io.Serializable

class Timeline: Serializable {
    val nombre: String?=""
    val fecha:String?=""
    val fechaInicio:String?=""
    val fechFin:String?=""
    val numeroFechas:Number?=null
    val periodo:String?=""
    val regiones: List<Regiones>?=null
}